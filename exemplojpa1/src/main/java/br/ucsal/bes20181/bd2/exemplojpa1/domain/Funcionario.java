package br.ucsal.bes20181.bd2.exemplojpa1.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.ucsal.bes20181.bd2.exemplojpa1.converters.SituacaoFuncionarioConverter;

@Entity
// @SequenceGenerator(name="", sequenceName="", initialValue=, allocationSize=,
// schema="")
public class Funcionario {

	@Id
	// @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="")
	// GenerationType.AUTO
	// GenerationType.IDENTITY
	// GenerationType.TABLE
	private Integer matricula;

	private String nome;

	@Embedded
	private Endereco endereco;

	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	@ElementCollection
	@CollectionTable(name = "tab_func_tel", joinColumns = @JoinColumn(name = "matricula_func"))
	private List<String> telefones;

	// @Enumerated(EnumType.STRING)
	@Convert(converter = SituacaoFuncionarioConverter.class)
	private SituacaoFuncionarioEnum situacao;

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public SituacaoFuncionarioEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoFuncionarioEnum situacao) {
		this.situacao = situacao;
	}

}
