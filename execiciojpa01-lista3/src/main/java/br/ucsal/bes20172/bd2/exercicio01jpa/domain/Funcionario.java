package br.ucsal.bes20172.bd2.exercicio01jpa.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ucsal.bes20172.bd2.exercicio01jpa.validation.ValidadorGrupo1;

@Entity
@Table(name = "tab_funcionario")
@Inheritance(strategy = InheritanceType.JOINED)
public class Funcionario {

	@Id
	@Column(length = 11)
	private String cpf;

	@NotNull(groups = ValidadorGrupo1.class)
	@Size(min = 5, max = 40, groups = { ValidadorGrupo1.class })
	@Column(nullable = false, length = 40)
	// @NotEmpty
	private String nome;

	@Column(nullable = true, length = 15)
	private String rg;

	@Column(name = "rg_orgao_expedidor", nullable = true, length = 50)
	private String rgOrgaoExpedidor;

	// FIXME utilizar a classe Estado no lugar de String
	@Column(name = "rg_uf", nullable = true, length = 2)
	private String rgUf;

	@Column(name = "jaca", nullable = false, length = 11)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "melancia", joinColumns = @JoinColumn(name = "manga"), foreignKey = @ForeignKey(name = "caju2"))
	private List<String> telefones;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	@Embedded
	private Endereco endereco;

	public Funcionario() {
	}

	public Funcionario(String cpf,
			@NotNull(groups = ValidadorGrupo1.class) @Size(min = 5, max = 40, groups = ValidadorGrupo1.class) String nome,
			String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones, Date dataNascimento,
			Endereco endereco) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.rg = rg;
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
		this.rgUf = rgUf;
		this.telefones = telefones;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		result = prime * result + ((rgOrgaoExpedidor == null) ? 0 : rgOrgaoExpedidor.hashCode());
		result = prime * result + ((rgUf == null) ? 0 : rgUf.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (rg == null) {
			if (other.rg != null)
				return false;
		} else if (!rg.equals(other.rg))
			return false;
		if (rgOrgaoExpedidor == null) {
			if (other.rgOrgaoExpedidor != null)
				return false;
		} else if (!rgOrgaoExpedidor.equals(other.rgOrgaoExpedidor))
			return false;
		if (rgUf == null) {
			if (other.rgUf != null)
				return false;
		} else if (!rgUf.equals(other.rgUf))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [cpf=" + cpf + ", nome=" + nome + ", rg=" + rg + ", rgOrgaoExpedidor=" + rgOrgaoExpedidor
				+ ", rgUf=" + rgUf + ", telefones=" + telefones + ", dataNascimento=" + dataNascimento + ", endereco="
				+ endereco + "]";
	}

}
