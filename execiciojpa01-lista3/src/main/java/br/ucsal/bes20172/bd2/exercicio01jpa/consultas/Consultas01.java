package br.ucsal.bes20172.bd2.exercicio01jpa.consultas;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.ucsal.bes20172.bd2.exercicio01jpa.SituacaoVendedor;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Estado;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.PessoaJuridica;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Vendedor;

public class Consultas01 {

	private static EntityManagerFactory emf;

	private static EntityManager em;

	public static void main(String[] args) {
		try {
			emf = Persistence.createEntityManagerFactory("exercicio01jpa");
			em = emf.createEntityManager();
			DadosBase.popularBase(em);
			em.clear();
			consultaClientesPorVendedor("Pedro");
			consultaClientesPorVendedorVersao2("Pedro");
			consultaVendedoresRamoAtividade("Alimentos");
			consultaVendedoresAtivos();
			consultaEstadosSemCidade();
			consultaQuantidadeVendedoresPorCidade();
			consultaVendoresDaCidade("Salvador");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static void consultaVendoresDaCidade(String nomeCidade) {
		TypedQuery<Vendedor> query = em
				.createQuery("select v from Vendedor v where v.endereco.cidade.nome = :nomeCidade", Vendedor.class);
		query.setParameter("nomeCidade", nomeCidade);
		List<Vendedor> vendedores = query.getResultList();
		System.out.println("******************************");
		System.out.println("Vendedores da cidade " + nomeCidade + ":");
		for (Vendedor vendedor : vendedores) {
			System.out.println(vendedor.getNome() + "|" + vendedor.getTelefones());
		}
	}

	private static void consultaQuantidadeVendedoresPorCidade() {
		TypedQuery<Object[]> query = em.createQuery(
				"select v.endereco.cidade.nome, count(*) from Vendedor v group by v.endereco.cidade.nome",
				Object[].class);
		List<Object[]> objetos = query.getResultList();
		System.out.println("******************************");
		System.out.println("Vendedores por cidade:");
		for (Object[] objeto : objetos) {
			System.out.println(objeto[0] + "|" + objeto[1]);
		}
	}

	private static void consultaEstadosSemCidade() {
		// TypedQuery<Estado> query = em.createQuery("select e from Estado e
		// where e.cidades.size = 0", Estado.class);
		TypedQuery<Estado> query = em.createQuery(
				"select e from Estado e where e.sigla not in (select c.estado.sigla from Cidade c)", Estado.class);
		List<Estado> estados = query.getResultList();
		System.out.println("******************************");
		System.out.println("Estados sem cidade:");
		for (Estado estado : estados) {
			System.out.println(estado.getSigla() + "|" + estado.getNome());
		}
	}

	private static void consultaVendedoresAtivos() {
		// TypedQuery<Vendedor> query1 = em.createQuery("select v from Vendedor
		// v where v.situacao =
		// br.ucsal.bes20172.bd2.exercicio01jpa.SituacaoVendedor.ATIVO",
		// Vendedor.class);
		TypedQuery<Vendedor> query = em.createQuery("select v from Vendedor v where v.situacao = :situacao",
				Vendedor.class);
		query.setParameter("situacao", SituacaoVendedor.ATIVO);
		List<Vendedor> vendedores = query.getResultList();
		System.out.println("******************************");
		System.out.println("Vendedores ativos:");
		for (Vendedor vendedor : vendedores) {
			System.out.println(vendedor.getCpf() + "|" + vendedor.getNome());
		}
	}

	private static void consultaVendedoresRamoAtividade(String nomeRamoAtividade) {
		TypedQuery<Vendedor> query = em.createQuery(
				"select distinct v from Vendedor v inner join v.clientes c inner join c.ramosAtividade r where r.nome = :nomeRamoAtividade order by v.nome",
				Vendedor.class);
		query.setParameter("nomeRamoAtividade", nomeRamoAtividade);
		List<Vendedor> vendedores = query.getResultList();
		System.out.println("******************************");
		System.out.println("Vendedores que atendem ao ramo de atividade " + nomeRamoAtividade + ":");
		for (Vendedor vendedor : vendedores) {
			System.out.println(vendedor.getNome() + "|" + vendedor.getTelefones() + "|" + vendedor.getEndereco());
		}
	}

	private static void consultaClientesPorVendedor(String nomeVendedor) {
		TypedQuery<Vendedor> query = em.createQuery("select v from Vendedor v where v.nome = :nomeVendedor",
				Vendedor.class);
		query.setParameter("nomeVendedor", nomeVendedor);
		Vendedor vendedor = query.getSingleResult();
		System.out.println("******************************");
		System.out.println("Clientes do vendedor " + nomeVendedor + ":");
		for (PessoaJuridica cliente : vendedor.getClientes()) {
			System.out.println("Nome:" + cliente.getNome()
					+ " Endereço: (não existe campo de endereço no cadastro de cliente)");
		}
	}

	private static void consultaClientesPorVendedorVersao2(String nomeVendedor) {
		TypedQuery<PessoaJuridica> query = em.createQuery(
				"select c from Vendedor v inner join v.clientes c where v.nome = :nomeVendedor", PessoaJuridica.class);
		query.setParameter("nomeVendedor", nomeVendedor);
		List<PessoaJuridica> clientes = query.getResultList();
		System.out.println("******************************");
		System.out.println("Clientes do vendedor " + nomeVendedor + ":");
		for (PessoaJuridica cliente : clientes) {
			System.out.println("Nome:" + cliente.getNome()
					+ " Endereço: (não existe campo de endereço no cadastro de cliente)");
		}
	}

}
