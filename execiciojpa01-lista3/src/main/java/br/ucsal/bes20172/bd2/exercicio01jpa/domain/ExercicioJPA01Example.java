package br.ucsal.bes20172.bd2.exercicio01jpa.domain;

public class ExercicioJPA01Example {

	public static void main(String[] args) {
		//
		// EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicio01jpa");
		//
		// EntityManager em = emf.createEntityManager();
		//
		// Vendedor vendedor = new Vendedor();
		// vendedor.cpf = "123";
		// vendedor.nome = "cla";
		// vendedor.dataNascimento = new Date();
		// // vendedor.endereco = new Endereco();
		// // vendedor.endereco.logradouro = "Rua X";
		// // vendedor.endereco.bairro = "Bairro Y";
		// vendedor.percentualComissao = new BigDecimal("5.1");
		// vendedor.situacao = SituacaoVendedor.SUSPENSO;
		// // vendedor.endereco.cidade = new Cidade();
		// // vendedor.endereco.cidade.sigla = "SSA";
		//
		// // Funcionario funcionario1 = new Funcionario();
		// //
		// // funcionario1.cpf = "123";
		// // funcionario1.nome = "";
		//
		// // ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		// // Validator validator = factory.getValidator();
		// // Set<ConstraintViolation<Funcionario>> erros = validator.validate(funcionario1);
		// // System.out.println(erros);
		// //
		// // if (erros.size() == 0) {
		//
		// // ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		// // Validator validator = factory.getValidator();
		// // Set<ConstraintViolation<Vendedor>> constraints = validator.validate(vendedor, ValidadorGrupo1.class,
		// // ValidadorGrupo2.class);
		// // for (ConstraintViolation<Vendedor> constraint : constraints) {
		// // System.out.println(constraint.getPropertyPath() + " " + constraint.getMessage());
		// // }
		// /*
		// * try {
		// *
		// * em.flush();
		// *
		// * } catch (ConstraintViolationException e) { System.out.println(e.getClass().getCanonicalName()); //
		// * for(ConstraintViolation<?> violation : e.getConstraintViolations()){ //
		// * System.out.println(violation.getMessage()); // } }
		// */
		// // }
		//
		// RamoAtividade ramoAtividade = new RamoAtividade();
		// ramoAtividade.nome = "caju";
		//
		// Produto produto = new Produto();
		// produto.nome = "jaca";
		//
		// em.getTransaction().begin();
		// //
		// // em.persist(vendedor);
		// //
		// em.persist(ramoAtividade);
		// em.persist(produto);
		// em.getTransaction().commit();
		//
		// em.find(Vendedor.class, "123");
		//
		// emf.close();
		//
	}

}
