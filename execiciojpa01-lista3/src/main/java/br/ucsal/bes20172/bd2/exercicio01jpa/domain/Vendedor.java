package br.ucsal.bes20172.bd2.exercicio01jpa.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ucsal.bes20172.bd2.exercicio01jpa.SituacaoVendedor;
import br.ucsal.bes20172.bd2.exercicio01jpa.converters.SituacaoConverter;
import br.ucsal.bes20172.bd2.exercicio01jpa.validation.ValidadorGrupo1;
import br.ucsal.bes20172.bd2.exercicio01jpa.validation.ValidadorGrupo2;

@Entity
public class Vendedor extends Funcionario {

	@Column(nullable = false, columnDefinition = "numeric(10,2) check (percentualComissao >= 0) ")
	// @DecimalMin("0.00")
	@Min(value = 0, message = "o valor ${validatedValue} é menor do que o {value}", groups = ValidadorGrupo2.class)
	private BigDecimal percentualComissao;

	@Convert(converter = SituacaoConverter.class)
	@Column(nullable = false, columnDefinition = "char(1) default 'A'")
	private SituacaoVendedor situacao;

	@ManyToMany(mappedBy = "vendedores")
	private List<PessoaJuridica> clientes;

	public Vendedor() {
	}

	public Vendedor(String cpf,
			@NotNull(groups = ValidadorGrupo1.class) @Size(min = 5, max = 40, groups = ValidadorGrupo1.class) String nome,
			String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefone, Date dataNascimento,
			Endereco endereco,
			@Min(value = 0, message = "o valor ${validatedValue} é menor do que o {value}", groups = ValidadorGrupo2.class) BigDecimal percentualComissao,
			SituacaoVendedor situacao, List<PessoaJuridica> clientes) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefone, dataNascimento, endereco);
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public BigDecimal getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(BigDecimal percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedor getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedor situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clientes == null) ? 0 : clientes.hashCode());
		result = prime * result + ((percentualComissao == null) ? 0 : percentualComissao.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (clientes == null) {
			if (other.clientes != null)
				return false;
		} else if (!clientes.equals(other.clientes))
			return false;
		if (percentualComissao == null) {
			if (other.percentualComissao != null)
				return false;
		} else if (!percentualComissao.equals(other.percentualComissao))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vendedor [percentualComissao=" + percentualComissao + ", situacao=" + situacao + ", clientes="
				+ clientes + "]";
	}

}
