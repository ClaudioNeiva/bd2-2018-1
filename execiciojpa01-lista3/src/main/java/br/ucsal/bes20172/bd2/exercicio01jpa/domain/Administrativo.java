package br.ucsal.bes20172.bd2.exercicio01jpa.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ucsal.bes20172.bd2.exercicio01jpa.validation.ValidadorGrupo1;

@Entity
public class Administrativo extends Funcionario {

	@Column(nullable = true)
	private Integer turno;

	public Administrativo() {
	}

	public Administrativo(String cpf,
			@NotNull(groups = ValidadorGrupo1.class) @Size(min = 5, max = 40, groups = ValidadorGrupo1.class) String nome,
			String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones, Date dataNascimento,
			Endereco endereco, Integer turno) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((turno == null) ? 0 : turno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Administrativo other = (Administrativo) obj;
		if (turno == null) {
			if (other.turno != null)
				return false;
		} else if (!turno.equals(other.turno))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Administrativo [turno=" + turno + "]";
	}

}
