package br.ucsal.bes20172.bd2.exercicio01jpa.consultas;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.ucsal.bes20172.bd2.exercicio01jpa.SituacaoVendedor;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Administrativo;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Cidade;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Endereco;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Estado;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Funcionario;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.PessoaJuridica;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.RamoAtividade;
import br.ucsal.bes20172.bd2.exercicio01jpa.domain.Vendedor;

public class DadosBase {

	private static final DateFormat dtf = new SimpleDateFormat("dd/MM/yyyy");

	public static void popularBase(EntityManager em) throws ParseException {
		em.getTransaction().begin();
		List<Estado> estados = criarEstados();
		List<Cidade> cidades = criarCidades(estados);
		List<RamoAtividade> ramosAtividade = criarRamosAtividade();
		List<PessoaJuridica> pessoasJuridicas = criarPessoasJuridicas(ramosAtividade);
		List<Funcionario> funcionarios = new ArrayList<>();
		funcionarios.addAll(criarVendedores(pessoasJuridicas, cidades));
		funcionarios.addAll(criarFuncionariosAdministrativos(cidades));
		estados.forEach(estado -> em.persist(estado));
		cidades.forEach(cidade -> em.persist(cidade));
		ramosAtividade.forEach(ramoAtividade -> em.persist(ramoAtividade));
		pessoasJuridicas.forEach(pessoaJuridica -> em.persist(pessoaJuridica));
		funcionarios.forEach(funcionario -> em.persist(funcionario));
		em.getTransaction().commit();
	}

	private static List<Estado> criarEstados() {
		List<Estado> estados = new ArrayList<>();
		estados.add(new Estado("BA", "Bahia", new ArrayList<>()));
		estados.add(new Estado("SE", "Sergipe", new ArrayList<>()));
		estados.add(new Estado("PR", "Paraná", new ArrayList<>()));
		return estados;
	}

	private static List<Cidade> criarCidades(List<Estado> estados) {
		List<Cidade> cidades = new ArrayList<>();
		cidades.add(new Cidade("SSA", "Salvador", estados.get(0)));
		cidades.add(new Cidade("FRS", "Feira de Santana", estados.get(0)));
		cidades.add(new Cidade("ALG", "Alagoinhas", estados.get(0)));
		estados.get(0).getCidades().add(cidades.get(0));
		estados.get(0).getCidades().add(cidades.get(1));
		estados.get(0).getCidades().add(cidades.get(2));
		cidades.add(new Cidade("LRT", "Lagarto", estados.get(1)));
		cidades.add(new Cidade("ARC", "Aracajú", estados.get(1)));
		estados.get(1).getCidades().add(cidades.get(3));
		estados.get(1).getCidades().add(cidades.get(4));
		return cidades;
	}

	private static List<RamoAtividade> criarRamosAtividade() {
		List<RamoAtividade> ramosAtividade = new ArrayList<>();
		ramosAtividade.add(new RamoAtividade("Comercio"));
		ramosAtividade.add(new RamoAtividade("Alimentos"));
		ramosAtividade.add(new RamoAtividade("Metalurgia"));
		return ramosAtividade;
	}

	private static List<PessoaJuridica> criarPessoasJuridicas(List<RamoAtividade> ramosAtividade) {
		List<PessoaJuridica> pessoasJuridicas = new ArrayList<>();
		pessoasJuridicas.add(new PessoaJuridica("33.014.556/0001-96", "Lojas Americanas S.A.", new ArrayList<>(),
				new BigDecimal("17495900000"), new ArrayList<>()));
		pessoasJuridicas.add(new PessoaJuridica("33.041.260/ 0652.90", "Via Varejo S.A.", new ArrayList<>(),
				new BigDecimal("26321000000"), new ArrayList<>()));
		pessoasJuridicas.add(new PessoaJuridica("02.916.265/0001-60", "JBS S.A.", new ArrayList<>(),
				new BigDecimal("165337000000"), new ArrayList<>()));
		pessoasJuridicas.add(new PessoaJuridica("01.838.723/0374-70", "Brasil Foods S.A.", new ArrayList<>(),
				new BigDecimal("33862900000"), new ArrayList<>()));
		pessoasJuridicas.get(0).getRamosAtividade().add(ramosAtividade.get(0));
		pessoasJuridicas.get(1).getRamosAtividade().add(ramosAtividade.get(0));
		pessoasJuridicas.get(3).getRamosAtividade().add(ramosAtividade.get(1));
		pessoasJuridicas.get(2).getRamosAtividade().add(ramosAtividade.get(1));
		return pessoasJuridicas;
	}

	private static List<Vendedor> criarVendedores(List<PessoaJuridica> clientes, List<Cidade> cidades)
			throws ParseException {
		List<Vendedor> vendedores = new ArrayList<>();
		vendedores.add(new Vendedor("11111", "Antonio", "123", "SSP", "BA", new ArrayList<>(), dtf.parse("01/05/1980"),
				new Endereco("Rua A", "Bairro 1", cidades.get(0)), new BigDecimal("5"), SituacaoVendedor.ATIVO,
				new ArrayList<>()));
		vendedores.add(new Vendedor("22222", "Claudio", "124", "SSP", "BA", new ArrayList<>(), dtf.parse("06/07/1990"),
				new Endereco("Rua B", "Bairro 1", cidades.get(0)), new BigDecimal("5"), SituacaoVendedor.ATIVO,
				new ArrayList<>()));
		vendedores.add(new Vendedor("33333", "Maria", "125", "SSP", "SP", new ArrayList<>(), dtf.parse("05/05/1970"),
				new Endereco("Rua C", "Bairro 1", cidades.get(0)), new BigDecimal("12"), SituacaoVendedor.SUSPENSO,
				new ArrayList<>()));
		vendedores.add(new Vendedor("44444", "Clara", "126", "CRM", "BA", new ArrayList<>(), dtf.parse("15/10/1971"),
				new Endereco("Rua D", "Bairro 2", cidades.get(3)), new BigDecimal("8"), SituacaoVendedor.ATIVO,
				new ArrayList<>()));
		vendedores.add(new Vendedor("55555", "Pedro", "127", "SSP", "RJ", new ArrayList<>(), dtf.parse("25/03/1977"),
				new Endereco("Rua E", "Bairro 3", cidades.get(4)), new BigDecimal("15"), SituacaoVendedor.ATIVO,
				new ArrayList<>()));

		vendedores.get(0).getTelefones().add("7778881");
		vendedores.get(1).getTelefones().add("8889992");
		vendedores.get(2).getTelefones().add("2223338");
		vendedores.get(3).getTelefones().add("1112226");
		vendedores.get(4).getTelefones().add("1122334");

		vendedores.get(0).getClientes().add(clientes.get(0));
		vendedores.get(0).getClientes().add(clientes.get(2));
		vendedores.get(0).getClientes().add(clientes.get(3));
		vendedores.get(1).getClientes().add(clientes.get(2));
		vendedores.get(4).getClientes().add(clientes.get(0));
		vendedores.get(4).getClientes().add(clientes.get(3));
		
		clientes.get(0).getVendedores().add(vendedores.get(0));
		clientes.get(0).getVendedores().add(vendedores.get(4));
		clientes.get(2).getVendedores().add(vendedores.get(0));
		clientes.get(2).getVendedores().add(vendedores.get(1));
		clientes.get(3).getVendedores().add(vendedores.get(0));
		clientes.get(3).getVendedores().add(vendedores.get(4));
		return vendedores;
	}

	private static List<Administrativo> criarFuncionariosAdministrativos(List<Cidade> cidades) throws ParseException {
		List<Administrativo> funcionariosAdministrativos = new ArrayList<>();
		funcionariosAdministrativos.add(new Administrativo("77777", "Joaquim", "456", "SSP", "RJ", new ArrayList<>(),
				dtf.parse("12/03/1987"), new Endereco("Rua E", "Bairro 1", cidades.get(0)), 2));
		funcionariosAdministrativos.add(new Administrativo("88888", "José", "135", "SSP", "BA", new ArrayList<>(),
				dtf.parse("05/06/1976"), new Endereco("Rua E", "Bairro 1", cidades.get(0)), 2));
		funcionariosAdministrativos.add(new Administrativo("99999", "Celeste", "246", "SSP", "SP", new ArrayList<>(),
				dtf.parse("09/04/1981"), new Endereco("Rua E", "Bairro 1", cidades.get(0)), 2));
		return funcionariosAdministrativos;
	}

}
