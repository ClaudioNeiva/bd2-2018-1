package br.ucsal.bes20172.bd2.exercicio01jpa.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sequence2", sequenceName = "sq_produto", initialValue = 28, allocationSize = 1)
public class Produto {

	@Id
	@GeneratedValue(generator = "sequence2")
	Integer codigo;

	String nome;

	
	@PostPersist
	void posInsert() {
		System.out.println("CAJUAJASDJD1");
	}
	
	@PreUpdate
	void preInsert() {
		System.out.println("CAJUAJASDJD2");
	}

}
