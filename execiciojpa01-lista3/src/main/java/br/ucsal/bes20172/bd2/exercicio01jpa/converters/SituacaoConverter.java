package br.ucsal.bes20172.bd2.exercicio01jpa.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20172.bd2.exercicio01jpa.SituacaoVendedor;

public class SituacaoConverter implements AttributeConverter<SituacaoVendedor, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedor attribute) {
		if (SituacaoVendedor.ATIVO.equals(attribute)) {
			return "A";
		}
		if (SituacaoVendedor.SUSPENSO.equals(attribute)) {
			return "S";
		}
		return null;
	}

	@Override
	public SituacaoVendedor convertToEntityAttribute(String dbData) {
		if ("A".equals(dbData)) {
			return SituacaoVendedor.ATIVO;
		}
		if ("S".equals(dbData)) {
			return SituacaoVendedor.SUSPENSO;
		}
		return null;
	}

}
