package br.ucsal.bes20182.bd2.enums;

public enum SituacaoVendedorEnum {
	ATIVO("ATV"), SUSPENSO("SPN");

	String codigo;

	private SituacaoVendedorEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoVendedorEnum valueOfCodigo(String codigo) {
		for (SituacaoVendedorEnum situacaoVendedor : values()) {
			if (situacaoVendedor.getCodigo().equals(codigo)) {
				return situacaoVendedor;
			}
		}
		throw new IllegalArgumentException();
	}

}
